VANTED can be started for development with specified Add-on (see StartVantedWithAddon.java).
For distribution generate jar-file under use of ant-script "createAdd-on.xml" (in Eclipse right-click and choose "Run As -> Ant Build").
Jar-files can be installed from users by starting VANTED, choosing sidepanel "help -> settings", opening Addon-manager and  choosing "Install Add-on".